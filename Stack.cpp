#include "Stack.h"
#include "ListStack.h"
#include "VectorStack.h"
#include "StackImplementation.h"
#include <iostream>
#include <stdexcept>

Stack::Stack(StackContainer container) : _containerType(container){
    switch (container) {
	    case StackContainer::List:
		    this->_pimpl = static_cast<IStackImplementation*>(new ListStack);    // конкретизируйте под ваши конструкторы, если надо
        	    break;

    	    case StackContainer::Vector: 
		    this->_pimpl = static_cast<IStackImplementation*>(new VectorStack);    // конкретизируйте под ваши конструкторы, если надо
        	    break;

    	    default:
		    throw std::runtime_error("Неизвестный тип контейнера");
    }
}


Stack::Stack(Stack&& moveStack) noexcept {
	if (this == &moveStack) {
		return;
	}
	this->_containerType = moveStack._containerType;
	this->_pimpl = moveStack._pimpl;
	moveStack._pimpl = nullptr;
}

Stack& Stack::operator=(Stack&& moveStack) noexcept {
	if (this == &moveStack) {
		return *this;
	}
	this->_containerType = moveStack._containerType;
	this->_pimpl = moveStack._pimpl;
	moveStack._pimpl = nullptr;
	return *this;
}

Stack::Stack(const ValueType* valueArray, const size_t arraySize, StackContainer container){
        this->_containerType = container;
	switch (container) {
		case StackContainer::List:
		    this->_pimpl = static_cast<IStackImplementation*>(new ListStack());    // конкретизируйте под ваши конструкторы, если надо
         	    break;

		case StackContainer::Vector: 
		    this->_pimpl = static_cast<IStackImplementation*>(new VectorStack());    // конкретизируйте под ваши конструкторы, если надо
        	    break;

    	    	default:
		    throw std::runtime_error("Неизвестный тип контейнера");
       }
       for (size_t i = 0; i < arraySize; i++){
	       this->_pimpl->push(valueArray[i]);
       }
}

Stack::Stack(const Stack& copyStack) {
	if (&copyStack == this) {
		return;
	}
	*this = copyStack;
}

Stack& Stack::operator=(const Stack& copyStack) {
    if(&copyStack == this) { 
	    return *this;
    }
    this->_containerType = copyStack._containerType;
    delete _pimpl;
    switch (this->_containerType) {
		case StackContainer::List: {
    			  ListStack* tmpThis = new ListStack( *static_cast<ListStack*>(copyStack._pimpl) );
		          this->_pimpl = static_cast<IStackImplementation*>(tmpThis);
			  break;
		}
			
		case StackContainer::Vector: {
			VectorStack* tmpThis = new VectorStack( *static_cast<VectorStack*>(copyStack._pimpl) );
		        this->_pimpl = static_cast<IStackImplementation*>(tmpThis);
			break;
		}

    	    	default:
		    throw std::runtime_error("Неизвестный тип контейнера");
    }
    return *this;
}

Stack::~Stack() {
    delete _pimpl;        // композиция!
}

void Stack::push(const ValueType& value) {
       _pimpl->push(value);
}

void Stack::pop() {
    try {
     	_pimpl->pop();
    }
    catch(...) {
	    throw std::invalid_argument("Error: steck");
    } 
}

const ValueType& Stack::top() const {
    try {
    	return _pimpl->top();
    }
    catch(...) {
	throw std::invalid_argument("Error: steck empty");
    } 
}

bool Stack::isEmpty() const { 
    return !_pimpl->isEmpty();
}

size_t Stack::size() const {
    return _pimpl->size();
}
