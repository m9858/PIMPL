#include "ListStack.h"


ListStack::ListStack(const ListStack& other) {
	if (&other == this) {
		return;
	}
	this->_data = other._data;
}

bool ListStack::isEmpty() const {
	return _data.size();
}

void ListStack::push(const ValueType& value) {
	_data.push_back(value);
}

size_t ListStack::size() const {
	return _data.size();
}

const ValueType& ListStack::top() const{
	if (_data.size()) {
		return _data.back();
	}
	throw std::invalid_argument("Error: steck empty");
}

void ListStack::pop() {
	if (_data.size()) {
		_data.pop_back(); 
		return;
	}
	throw std::invalid_argument("Error: steck empty");
}
