#include "Stack.h"
#include <ctime>
#include <iostream>


int main() {
	std::clock_t c_start = std::clock();
	std::cout << "Stack on vector" << std::endl;
	Stack a(StackContainer::Vector);
	for (int i = 0; i < 100000000; i++) {
		a.push(i);
	}
	a.top();
	for (int i = 0; i < 9000000; i++) {
		a.pop();
	}
	a.top();

	std::cout << "Time: " <<  (static_cast<double>(std::clock() - c_start) / CLOCKS_PER_SEC) << std::endl;
	std::cout << "Stack on List " << std::endl;
	c_start = std::clock();
	Stack a1(StackContainer::List);
	for (int i = 0; i < 100000000; i++) {
		a1.push(i);
	}
	a1.top();
	for (int i = 0; i < 9000000; i++) {
		a1.pop();
	}
	a1.top();
	std::cout << "Time: " <<  (static_cast<double>(std::clock() - c_start) / CLOCKS_PER_SEC) << std::endl;
	return 0;
}
