.PHONY: all, run, clear


all: main


main: ListStack.o main_test.o Stack.o Vector.o VectorStack.o
	g++ $^ -o main_test -g
%.o: %.cpp
	g++ -c $< -o $*.o -g
clean:
	rm -f *.o main_test

run: main
	./main_test

