#pragma once
#include "StackImplementation.h"
#include <list>
#include <stdexcept>

using ValueType = double;

class ListStack : public IStackImplementation {
	private:
		std::list<ValueType> _data;
	public:
		ListStack() = default;
		~ListStack() = default;
		ListStack(const ListStack& other);
		bool isEmpty() const;
		void push(const ValueType& value);
		void pop();
		const ValueType& top() const;
		size_t size() const;
};
