#include "VectorStack.h"

VectorStack::VectorStack(const VectorStack& other){
	if (this == &other){
		return;
	}
	this->_data = other._data;
}

void VectorStack::push(const ValueType& value) {
	this->_data.pushBack(value);
}

void VectorStack::pop() {
	if (this->_data.size()){
		this->_data.popBack();
		return;
	}
	throw std::invalid_argument("Error: steck empyt");
}

const ValueType& VectorStack::top() const {
	if (!this->_data.size()){
		throw std::invalid_argument("Error: steck empyt");
	}
	return this->_data[this->_data.size() - 1];
}

bool VectorStack::isEmpty() const {
	return this->_data.size();
}

size_t VectorStack::size() const {
	return this->_data.size();
}
