# PIMPL stack algorithm

The algorithm is executed using PIMPL (Opaque Pointer), implemented using a self-written vector and a single linked list and makes a stack.

### The commands that are in the stack

``` 
* push(number) - adding an element
* pop() - removing an element
* top() - getting the last element
```

## Creating a class

Creating a class using the **VECTOR** or **LINKED**\_**LIST** flag where PIMPL is **enum**
example
```
Stack name(StackContainer::Vector)  - creates a stack on Vecotr the default is always a vector
Stack name(PIMPL::List) - creates a stack on a singly linked list
```

## A little bit about the file main\_test.cpp and that
there are two types of stack implemented in it and time on the same tasks spoiler stack on vector works faster


## Сompilation 1.cpp and launch

to build the program there is **make** or **make run** if you also want to run it

### Via make 
```
make 
make run 

#You can do it like this

make run  

#So too

make
./main_test 
```

./PIMPL - file is executable 

Yes there is a clean in make **clean** to delete an executable file or an object 

### Manual assembly 

```
сс \*.cpp -o TestPim 
./TestPimp 
```

explanations to the first line cc the current c++ compiler \*.cp all files with extensions .cpp you can write yourself what you need and **-o the name of the executable file** 
