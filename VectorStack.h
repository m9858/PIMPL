#pragma once
#include "StackImplementation.h"
#include "Vector.h"
#include <stdexcept>


class VectorStack : public IStackImplementation {
	private:
		Vector _data;
	public:
		VectorStack() = default;
		~VectorStack() = default;
		VectorStack(const VectorStack& other);
		bool isEmpty() const;
		void push(const ValueType& value);
		void pop();
		const ValueType& top() const;
		size_t size() const;
};
